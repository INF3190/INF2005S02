Ce projet contient les exemples de la séance 2 du cours INF2005 --Programmation Web -- enseigné à l'UQAM par Johnny Tsheke.

La plupart de ces exemples sont sur HTML 5. La présentation du cours est sur moodle (INF2005-S02.pdf) https://www.moodle.uqam.ca. 

Visualisez les exemple avec un navigateur web de votre choix. Merci de nous parvenir vos commentaires et suggestions.